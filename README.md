# Sleet-Templates

“Simple HTML templates and themes for Sleek HTML websites”

---


### Why HTML code-based templates?

Because I see so many places where you can design a site without code.
Some people still like to have create a site from code, right?

### How to use.

Pick a template that you want to use.
Copy the template folder to wherever you want to store your website sites code.
Rename the folder to your website name.
Then open the folder with vs code or other favorite code editor..

### Team:

**Nonresistant Really**

Hi, I am a digital designer content creator.
You can find me on youtube and learn more about me on my website.
When I am not editing video footage I am probably working with code.
If you are looking for someone you can depend on to design you a webiste, give me a call!

---


# Some templates and themes and their descriptions...


**Blank Templates**

These would be a good place to start to see a blank basic template.
From these I create some amazing themes.

- [Blank Blank](templates/Blank-Blank)
  - Nothing is just blank. This is a template with just some starter code and files, but nothing else.
- [Blank Blank 2024](templates/Blank-Blank_2024)
  - When I initially started sleet templates I created muliple css files for each template. I found my self removing all those css files when I used the template, so that is why the fix. As I create and work on a specific project I do create muliple css files.
- [Multi Page Blank](templates/Multi-Page-Blank)
  - A blank template for creating a multi page website.
(You would not belive how many hours it took me to make this!)

---

**One Page Templates**

- [Flash Sale](templates/Flash-Sale)
  - This is a template with a colorful header and some changing color animations.

**Multi Page Templates**

- [Blue Bubble](templates//Blue-Bubble)
  - I named this blue bubble, because of the rounded semi-transparent fixed header. But obviously it could be any color. Hope you enjoy!
- [Sandwich Sam](templates/sandwich-sam)
  - No I didn't name this after some restaurant or my brother. I named it this becasue of the nav, though some people call it a hamburger menu.
- [Bright Photography](templates/bright-photography)
  - This could be multipage or single page.
  


---

Thanks for checking out Sleet Design!
